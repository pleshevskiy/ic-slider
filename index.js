/*global $, Node*/
var emptyFunction = function () {};

var defaultConfig = {
    $next: null,
    $prev: null,
    $paginate: null,
    paginateItemHTML: '<div></div>',
    activeClass: 'active',
    swipeble: true,
    isVertical: false,
    relationSlider: null,
    isManualControl: false,
    onBeforeInitialize: emptyFunction,
    onInitialize: emptyFunction,
    onBeforeChange: emptyFunction,
    onChange: emptyFunction,
    onBeforeResize: emptyFunction,
    onResizeList: emptyFunction,
    onAfterResize: emptyFunction,
    onItemsLoaded: emptyFunction,
    onRelationItemsLoaded: emptyFunction,
    onInitItemByIndex: emptyFunction
};


function Slider(rootSelector, config) {
    this._config = Object.assign({}, defaultConfig, config);
    this.config = this.getConfigWithRules();
    if (!this.config.listSelector) {
        this.config.listSelector = rootSelector.trim() + '__list';
    }
    if (this.config.relationSlider
        && !(this.config.relationSlider instanceof Slider)) {
        console.warn('Relation must be Slider');
        this.config.relationSlider = null;
    }

    this.$root = $(rootSelector);
    
    // Slides
    this.$list = this.$root.find(this.config.listSelector);
    this.$listWrap = this.$list.parent();
    this.$items = this.$list.children();
    this.$all = this.$root.add(this.$items).add(this.$list).add(this.$listWrap);
    
    // Navigation
    this.$prevButton = $(this.config.$prev);
    this.$nextButton = $(this.config.$next);
    
    // Pagination
    this.$paginate = $(this.config.$paginate);
    this.$paginateItems = this.$paginate.children();

    this.__initialized = false;

    this.initialize();
}

Slider.prototype.initialize = function () {
    if (!this.$list.length) {
        throw new Error('Not found items list for slider');
    }

    this.$all.css('transition', 'none');
    this.config.onBeforeInitialize.call(this);

    this.resizeList();
    this.initItems();
    this.changeActiveItemByIndex(0);
    this.attachEvents();

    this.__initialized = true;
    this.config.onInitialize.call(this);
    this.$all.css('transition', '');
};

Slider.prototype.attachEvents = function () {
    if (this.__initialized)
        return;

    this.__isItemClickDisabled = false;

    var that = this;
    var rSlider = this.config.relationSlider;
    if (rSlider) {
        rSlider.$items.on('click', function (e) {
            e.preventDefault();
            if (rSlider.__isItemClickDisabled) {
                return;
            }
            that.changeActiveItemByIndex($(this).index());
        });

        rSlider.$root.on({
            'items.loaded': function () {
                that.config.onRelationItemsLoaded.call(that);
                that.changeActiveItemByIndex(that.getActiveIndex());
            },
            'item.prev': function () {
                that.prevItem();
            },
            'item.next': function () {
                that.nextItem();
            }
        });
    }

    this.$root.on({
        'items.loaded': function () {
            if (rSlider) {
                rSlider.resizeList();
                rSlider.initItems();
                rSlider.changeActiveItemByIndex(rSlider.getActiveIndex());
            }

            that.resizeList();
            that.initItems();
            that.changeActiveItemByIndex(that.getActiveIndex());
        },
        'item.prev': function () {
            if (rSlider)
                rSlider.prevItem();
            that.prevItem();
        },
        'item.next': function () {
            if (rSlider)
                rSlider.nextItem();
            that.nextItem();
        }
    });
    
    this.$paginate.on('click', '>*', function (e) {
        e.preventDefault();
        
        var index = $(this).index();
        if (rSlider) {
            rSlider.changeActiveItemByIndex(index);
        }
        
        that.changeActiveItemByIndex(index);
    });

    this.$items.on('click', function (e) {
        e.preventDefault();
        if (that.__isItemClickDisabled) {
            return;
        }

        var index = $(this).index();
        if (rSlider) {
            rSlider.changeActiveItemByIndex(index);
        }

        that.changeActiveItemByIndex(index);
    });

    this.$prevButton.on('click', function (e) {
        e.preventDefault();
        that.$root.trigger('item.prev');
    });

    this.$nextButton.on('click', function (e) {
        e.preventDefault();
        that.$root.trigger('item.next');
    });

    if (this.config.swipeble) {
        var touchStartCoord, touchDiff;
        this.$listWrap.on({
            'touchstart mousedown': function (e) {
                touchStartCoord = that.getTouchCoordByEvent(e.originalEvent);

                var isTouch = e.type === 'touchstart';
                var startEventNames = isTouch ? 'touchmove' : 'mousemove';
                var endEventNames = isTouch ? 'touchend touchcancel' : 'mouseup';

                function onTouchMove(e) {
                    var touchCoord = that.getTouchCoordByEvent(e.originalEvent);
                    touchDiff = touchStartCoord - touchCoord;
                }

                function onTouchEnd() {
                    $(document).off(startEventNames, onTouchMove);
                    $(document).off(endEventNames, onTouchEnd);

                    if (touchDiff && Math.abs(touchDiff) > 100) {
                        that.__isItemClickDisabled = true;
                        if (touchDiff > 0) {
                            that.$root.trigger('item.next');
                        } else {
                            that.$root.trigger('item.prev');
                        }

                        setTimeout(function () {
                            that.__isItemClickDisabled = false;
                        }, 10);
                    }
                }

                $(document).on(startEventNames, onTouchMove);
                $(document).on(endEventNames, onTouchEnd);
            },

        });
    }

    $(window).on('resize', function () {
        that.$all.css('transition', 'none');
        that.config = that.getConfigWithRules();
        that.$items.css('width', '');
        that.config.onBeforeResize.call(that);
        that.resizeList();
        that.initItems();
        that.config.onAfterResize.call(that);
        that.changeActiveItemByIndex(that.getActiveIndex());
        that.$all.css('transition', '');
    });
};

Slider.prototype.resizeList = function () {
    var that = this;

    var $imgs = this.$list.find('img');
    if ($imgs.length && !this.__imagesLoaded && !this.__imagesLoadedProcess) {
        this.__imagesLoadedProcess = true;
        var count = 0;
        $imgs.on('load', function () {
            if (++count === $imgs.length - 1) {
                that.__imagesLoadedProcess = false;
                that.__imagesLoaded = true;
                that.config.onItemsLoaded.call(this);
                that.$root.trigger('items.loaded');
            }
        }).each(function () {
            this.src = this.src;
        });
        return;
    }
    
    this.config.onResizeList.call(this);
    if (this.config.isManualControl) {
        return;
    }

    this.$items.css('position', 'relative');
    this.$listWrap.css('height', '');
    this.$listWrap.css('width', '');
    this.$list.css('width', '');
    this.$list.css('height', '');


    if (this.config.isVertical) {
        this.$list.height(this.getOffsetByItems(this.$items));
        var maxItemWidth = Math.max.apply(
            null,
            [].map.call(this.$items, function (node) {
                return $(node).outerWidth();
            })
        );
        this.$list.width(maxItemWidth);
        this.$listWrap.width(maxItemWidth);
    } else {
        this.$list.width(this.getOffsetByItems(this.$items));
        var maxItemHeight = Math.max.apply(
            null,
            [].map.call(this.$items, function (node) {
                return $(node).outerHeight();
            })
        );
        this.$list.height(maxItemHeight);
        this.$listWrap.height(maxItemHeight);
    }

    this.$items.css('position', 'absolute');
};

Slider.prototype.initItemByIndex = function (index) {
    this.config.onInitItemByIndex.call(this, index);

    if (!this.config.isManualControl) {
        if (this.config.isVertical) {
            this.$items.eq(index).css({
                left: 0,
                top: this.getOffsetByItemIndex(index)
            });
        } else {
            this.$items.eq(index).css({
                top: 0,
                left: this.getOffsetByItemIndex(index)
            });
        }
    }
};

Slider.prototype.changeActiveItemByIndex = function (index) {
    this.config.onBeforeChange.call(this, index);

    var $activeItem = this.$items.eq(index);
    $activeItem.addClass(this.config.activeClass)
        .siblings().removeClass(this.config.activeClass);
    if (this.$paginateItems.length === this.$items.length) {
        this.$paginateItems
            .eq(index).addClass(this.config.activeClass)
            .siblings().removeClass(this.config.activeClass);
    }

    var listSideSize = this.getSideSize(this.$list);
    var listWrapSideSize = this.getSideSize(this.$listWrap);
    var revertListOffset = this.getOffsetByItemIndex(index, true);
    var listOffset = this.getOffsetByItemIndex(index);
    var centerOffset = (listWrapSideSize - this.getFullSideSize($activeItem)) / 2;
    var activeItemOffset = centerOffset - listOffset;
    if (listSideSize >= listWrapSideSize) {
        if (activeItemOffset > 0) {
            activeItemOffset = 0;
        } else if (revertListOffset <= (listWrapSideSize + this.getFullSideSize($activeItem))/2) {
            activeItemOffset = listWrapSideSize - listSideSize;
        }
    } else {
        activeItemOffset = (listWrapSideSize - listSideSize) / 2;
    }

    if (!this.config.isManualControl) {
        if (this.config.isVertical) {
            this.$list.css({
                'left': '',
                'top': activeItemOffset
            });
        } else {
            this.$list.css({
                'top': '',
                'left': activeItemOffset
            });
        }
    }

    if (index === 0) {
        this.$prevButton.addClass('disabled');
    } else {
        this.$prevButton.removeClass('disabled');
    }

    if (index === this.$items.length - 1) {
        this.$nextButton.addClass('disabled');
    } else {
        this.$nextButton.removeClass('disabled');
    }

    this.config.onChange.call(this, index);
};

Slider.prototype.prevItem = function () {
    var activeItemIndex = this.getActiveIndex();
    if (activeItemIndex > 0) {
        this.changeActiveItemByIndex(activeItemIndex - 1);
    }
};

Slider.prototype.nextItem = function () {
    var activeItemIndex = this.getActiveIndex();
    if (activeItemIndex < this.$items.length - 1) {
        this.changeActiveItemByIndex(activeItemIndex + 1);
    }
};

Slider.prototype.initItems = function () {
    var that = this;
    this.$items.each(function (i) {
        that.initItemByIndex(that.$items.eq(i).index());
    });
    
    if (this.$paginate.length) {
        var html = [].reduce.call(this.$items, function (prev) {
            return prev + that.config.paginateItemHTML;
        }, '');
        
        if (this.$paginate.html() !== html) {
            this.$paginate.html(html);
            this.$paginateItems = this.$paginate.children();
            this.changeActiveItemByIndex(this.getActiveIndex());
        }
    }
};

Slider.prototype.getOffsetByItemIndex = function (index, isRevert) {
    var $items = isRevert ?
        this.$items.slice(index)
        : this.$items.slice(0, index);
    return this.getOffsetByItems($items);
};

Slider.prototype.getOffsetByItems = function ($items) {
    var that = this;
    return [].reduce.call($items, function (prev, node) {
        return prev + that.getFullSideSize(node);
    }, 0);
};

Slider.prototype.getActiveIndex = function () {
    return this.$list.find('.' + this.config.activeClass).index();
};

Slider.prototype.getSideSize = function ($node) {
    if ($node instanceof Node) {
        $node = $($node);
    }
    return this.config.isVertical ?
        $node.height()
        : $node.width();
};

Slider.prototype.getFullSideSize = function ($node) {
    if ($node instanceof Node) {
        $node = $($node);
    }
    return this.config.isVertical ?
        $node.outerHeight(true)
        : $node.outerWidth(true);
};

Slider.prototype.getTouchCoordByEvent = function (e) {
    if (e.changedTouches) {
        e = e.changedTouches[0];
    }
    return this.config.isVertical ? e.clientY : e.clientX;
};

Slider.prototype.getConfigWithRules = function () {
    var result = Object.assign({}, this._config, { rules: void 0 });
    var rules = this._config.rules;
    var windowWidth = window.innerWidth;
    if (rules && Array.isArray(rules)) {
        for (var i = 0; i < rules.length; i++) {
            var rule = rules[i];
            var condition;
            if (rule.gte != null) {
                condition = windowWidth >= rule.gte;
            } else if (rule.gt != null) {
                condition = windowWidth > rule.gt;
            } else if (rule.lte != null) {
                condition = windowWidth <= rule.lte;
            } else if (rule.lt != null) {
                condition = windowWidth < rule.lt;
            }

            if (condition && rule.config) {
                Object.assign(result, rule.config);
                break;
            }
        }
    }

    // console.log(result)
    return result;
};